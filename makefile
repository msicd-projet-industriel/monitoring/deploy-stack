lint:
	@rm -rf lint && mkdir lint
	@ansible-lint --nocolor > lint/ansible.log
	@yamllint . > lint/yamllint.log
