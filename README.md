# Déploiement de la stack Elastic sur l'orchestrateur Kubernetes pour le monitoring à partir d'un pipeline sur Gitlab  

__École__ : IMT Atlantique  
__Formation__ : Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__ : 2022-2023  
__UE__ : Projet industriel  
__Dépôt Gitlab__ : <https://gitlab.imt-atlantique.fr/projet-industriel/monitoring/deploy-stack>  
__Date__ : 27 Mars 2023  

## Description

Ce projet permet de déployer et maintenir automatiquement la stack Elastic sur l'infrastructure du monitoring.  

* L'infrastruction du projet "deploy-infrastructure" du groupe "monitoring-logging" doit être préalablement mise en place.  

* L'inventaire de l'infrastructure est généré par le projet "deploy-infrastructure"  

* L'orchestrateur du projet "deploy-orchestrator" du groupe "monitoring-logging" doit être préalablement mise en place.  

* Les opérations sont effectuées à partir des playbooks Ansible.  

* Les tâches à exécuter sont organisées dans des rôles.  

* Les playbooks sont variabilisés pour permettre la personnalisation de l'environnement de production.  

* La connexion SSH aux instances est configurée à l'aide du fichier __ssh.conf.template__ et des variables d'environnement. Le fichier __ssh.conf__ va être généré et utilisé par la configuration d'Ansible.  

* Un Dockerfile permet de construire l'image des Jobs  

## Prérequis

* Le déploiement de l'infrastructure du projet "deploy-infrastruction"  
* Le déploiement de l'orchestrateur du projet "deploy-orchestrator"  
* Le package registry du projet "deploy-infrastruction":  
  * la disponibilité du fichier __hosts.ini__ du package  
* La clé privée SSH de connexion aux instances  
* Un token d'accès à l'api du "projet-industriel" des droits "owner" pour assurer l'utilisation du package registry  

## Configurer le déploiement Ansible

* Modifier le fichier __group_vars/all/main.yml__ pour personnaliser les tâches à effectuer par Ansible.  
* Modifier les fichiers manisfests contenus dans le role __roles/manifests__ pour personnaliser la stack.  

> Remarque: Ne pas toucher au fichier __group_vars/all/env.yml.template__. Il est configuré dans la chaine de déploiement.  

## Configurer l'environnement du pipeline sur Gitlab

### Créer ou mettre à jour les variables Gitlab du groupe "monitoring-logging"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __PRIVATE_KEY__: contenu de la clé privée SSH (Utiliser une variable de type "File")  
  * __BASTION_USER__: utilisateur SSH bastion  
  * __NODE_USER__: utilisateur SSH des instances  
  * __BASTION_PUBLIC_IP__: adresse IP publique de l'instance bastion  
  * __TF_PROJECT_ID__: identifiant du projet deploy-infrastructure  
  * __TF_PKG_NAME__: nom du package registry des fichiers d'inventaire générés par deploy-infrastructure.  
  * __TF_PKG_VERSION__: version du package registry

### Créer ou mettre à jour les variables Gitlab du groupe parent "projet-industriel"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.
  * __API_TOKEN__: token d'accès à l'API Gitlab du groupe avec les privilèges "owner"  

### Vérifier l'inventaire

Il faut s'assurer que le fichier d'inventaire __hosts.ini__ de l'infrastructure est disponible dans le package registry __infra-observ__ du projet "deploy-infrastructure".  

## Déployer la stack Elastic dans l'orchestrateur Kubernetes de l'infrastructure de monitoring

* Se rendre dans l'onglet "CI/CD > pipeline" du projet.  
* Cliquer sur le bouton __"Run pipeline"__.  
* Sur la fénètre qui s'affiche, cliquer à nouveau sur le bouton __"Run pipeline"__ pour valider l'opération sans renseigner de variables.  

### Déployer Elasticsearch et Kibana

Déclencher le job __elastic-kibana__ pour déployer elasticsearch et kibana dans le pipeline  

Kibana est accessible à partir de l'adresse IP flottante de l'orchestrateur via le port 300000  
Elasticsearch quant à lui utilise le port 80 pour recevoir données des beats du cluster de l'application ICD Meteo  

### Déployer Kube state metrics, filebeat et metricbeat

S'assurer que kibana est disponible à partir du navigateur puis,  

* déclencher le job __state_metrics__ pour déployer kube state metrics  
* déclencher le job __beats__ pour déployer les beats  
